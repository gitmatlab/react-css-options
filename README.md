# CSS Options in React

These are several ways how to use CSS in React. You'll find little code examples and a list of the **The Good 😋  and the Bad 🤬**.

It's not an extensive evalutation and discussion of the options but it serves for a quick and easy overview how CSS can be done in React.



## Inline

- 😋 Quick and simple
- 🤬 If css gets sophisticated, then the code gets fast hard to read
- 🤬 Css camelCased and values in quotes

## Variable

- 😋 Quick and simple
- 😋 all in one file

The javascript object is quit limiting the css features.

- 🤬 no animations
- 🤬 no nested elements like: first child, all child elements etc.
- 🤬 no pseudo-classes (i.e. :hover, ::first-line ...)

## Import

- 😋 All features that CSS offers are available
- 😋 no naming conventions
- 😋 can be used with SASS and other tools
- 😋 often the css file is named after the component (Import.jsx & Import.css)

## StyledComponents

- 😋 encapsulated CSS
- 😋 full css feature set and syntax
- 😋 easy to include JS logic to styles (ie. color: ${condition ? 'brown' : 'blue'})
- 🤬 extra library: styled-components
- 🤬 backticks are awkward

## Tailwind

[tailwind docs](https://tailwindcss.com/docs/installation/using-postcss)

- 😋 css all in one file (might be good, might be bad)
- 🤬 extra library: tailwindcss
- 🤬 extra configuration: tailwind.config.js
- 🤬 extra css file with base modules: tailwind.css

## Module CSS

- name convention: `[name].module.css`. Example: './Button.module.css';
- this feature comes with newer versions of: react-scripts
- more about [CSS Modules](https://css-tricks.com/css-modules-part-1-need/)

- 😋 it scopes the styles to a module
- 😋 it automatically generates uniq class names, so we don't need to create long creasy names to avoid clashes. Example: 'font-size__serif--large' can be named to 'big', as long it's unique in your scope.


## PostCss

The postcss module parses the css and offers an API for plugnis. Examples of plugins are:
- autoprefixer: adds browser vedor prefixes. Usefull for animations and complex css features
- stylelint: linter for css
- postcss-scss

- 🤬 extra library: postcss, autoprefixer
- 🤬 plus plugin(s): autoprefixer

## Credits

- The project icon is an image taken from [wikimedia](https://commons.wikimedia.org/wiki/File:CSS3_logo_and_wordmark.svg). The autor is Rudloff and the image is licenced under [Creative Commons Attribution 3.0](https://creativecommons.org/licenses/by/3.0).
