export default function Variable() {
  const styles = {
    wrapper: { color: 'blue', fontFamily: 'fantasy' }
  }
  return (
      <div style={styles.wrapper}>
        <h1>Hello Variable World!</h1>
      </div>
  );
}