import Import from './Import';
import Inline from './Inline';
import Module from './Module';
import StyledComponents from './StyledComponents';
import Tailwind from './Tailwind';
import Variable from './Variable';

function App() {
  return (
    <div>
      <Inline />
      <Variable />
      <Import />
      <StyledComponents />
      <Tailwind />
      <Module />
    </div>
  );
}

export default App;
