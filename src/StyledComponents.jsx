import styled from 'styled-components'

export default function StyledComponents() {
  const condition = true

  const H1 = styled.h1`
    color: ${condition ? 'brown' : 'blue'};
    font-family: Times;
  `;

  return (
      <div>
        <H1>Hello Variable World!</H1>
      </div>
  );
}