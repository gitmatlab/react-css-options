// this is the main css for tailwind
// it is usually loaded on App root
import './tailwind.css'

export default function Tailwind() {
  return (
      <div>
        <h1 className="font-mono text-pink-400">Hello Tailwind World!</h1>
      </div>
  );
} 