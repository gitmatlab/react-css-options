import style from './Module.module.css'

export default function Module() {
  return (
      <div>
        <h1 className={style.headline}>Hello Module World!</h1>
      </div>
  );
}